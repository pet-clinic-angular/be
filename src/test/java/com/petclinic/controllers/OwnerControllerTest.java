package com.petclinic.controllers;

import com.petclinic.dto.OwnerDto;
import com.petclinic.mapper.OwnerMapper;
import com.petclinic.model.Owner;
import com.petclinic.services.OwnerService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {OwnerController.class})
@ExtendWith(SpringExtension.class)
class OwnerControllerTest {
    @Autowired
    private OwnerController ownerController;

    @MockBean
    private OwnerService ownerService;

    private Set<OwnerDto> activeOwnerSet;
    private Set<OwnerDto> inactiveOwnerSet;

    @BeforeEach
    public void init() {
        Owner activeOwner = new Owner();
        activeOwner.setId(1L);
        activeOwner.setFirstName("Jane");
        activeOwner.setLastName("Doe");
        activeOwner.setAddress("O Strada");
        activeOwner.setCity("Iasi");
        activeOwner.setTelephoneNumber("0711111111");
        activeOwner.setIsActive(true);
        activeOwner.setPets(new HashSet<>());

        activeOwnerSet = new HashSet<>();
        OwnerDto activeOwnerDto = OwnerMapper.ownerToOwnerDto(activeOwner);
        activeOwnerSet.add(activeOwnerDto);

        Owner inactiveOwner = new Owner();
        inactiveOwner.setId(2L);
        inactiveOwner.setFirstName("James");
        inactiveOwner.setLastName("Doe");
        inactiveOwner.setAddress("Alta Strada");
        inactiveOwner.setCity("Iasi");
        inactiveOwner.setTelephoneNumber("0722222222");
        inactiveOwner.setIsActive(false);
        inactiveOwner.setPets(new HashSet<>());

        inactiveOwnerSet = new HashSet<>();
        OwnerDto inactiveOwnerDto = OwnerMapper.ownerToOwnerDto(inactiveOwner);
        inactiveOwnerSet.add(inactiveOwnerDto);
    }

    @AfterEach
    public void teardown() {
        activeOwnerSet.clear();
        inactiveOwnerSet.clear();
    }

    @Test
    void testFindActiveOwners() throws Exception {
        when(ownerService.findActiveOwners()).thenReturn(activeOwnerSet);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/owners/find");
        MockMvcBuilders.standaloneSetup(ownerController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content()
                        .string("[{\"id\":1,\"firstName\":\"Jane\",\"lastName\":\"Doe\",\"address\":\"O Strada\",\"city\":\"Iasi\",\"phoneNumber\":\"0711111111\",\"pets\":[]}]"));
    }

    @Test
    void testFindInactiveOwners() throws Exception {
        when(ownerService.findInactiveOwners()).thenReturn(inactiveOwnerSet);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/owners/inactive");
        MockMvcBuilders.standaloneSetup(ownerController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content()
                        .string("[{\"id\":2,\"firstName\":\"James\",\"lastName\":\"Doe\",\"address\":\"Alta Strada\",\"city\":\"Iasi\",\"phoneNumber\":\"0722222222\",\"pets\":[]}]"));
    }

    @Test
    void testChangeOwnerIsActiveFlag() throws Exception {
        doNothing().when(ownerService).changeOwnerIsActiveFlag(any());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.patch("/api/change-flag/{id}", 123L);
        MockMvcBuilders.standaloneSetup(ownerController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
