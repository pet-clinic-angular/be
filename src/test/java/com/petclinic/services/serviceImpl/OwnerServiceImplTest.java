package com.petclinic.services.serviceImpl;

import com.petclinic.exceptions.EmptyOwnerListException;
import com.petclinic.exceptions.NoOwnerFoundException;
import com.petclinic.model.Owner;
import com.petclinic.repositories.OwnerRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {OwnerServiceImpl.class})
@ExtendWith(SpringExtension.class)
class OwnerServiceImplTest {
    @MockBean
    private OwnerRepository ownerRepository;

    @Autowired
    private OwnerServiceImpl ownerServiceImpl;

    private List<Owner> ownerList;
    private List<Owner> inactiveOwnerList;

    @BeforeEach
    public void init() {
        Owner ownerActive = new Owner();
        ownerActive.setId(11L);
        ownerActive.setFirstName("Jane");
        ownerActive.setLastName("Doe");
        ownerActive.setAddress("O Strada");
        ownerActive.setCity("Iasi");
        ownerActive.setTelephoneNumber("0711111111");
        ownerActive.setIsActive(true);
        ownerActive.setPets(new HashSet<>());

        Owner ownerActive1 = new Owner();
        ownerActive1.setId(22L);
        ownerActive1.setFirstName("James");
        ownerActive1.setLastName("Doe");
        ownerActive1.setAddress("Alta Strada");
        ownerActive1.setCity("Iasi");
        ownerActive1.setTelephoneNumber("0722222222");
        ownerActive1.setIsActive(true);
        ownerActive1.setPets(new HashSet<>());

        ownerList = new ArrayList<>();
        ownerList.add(ownerActive);
        ownerList.add(ownerActive1);

        Owner ownerInactive = new Owner();
        ownerInactive.setId(12L);
        ownerInactive.setFirstName("Jane");
        ownerInactive.setLastName("Doe");
        ownerInactive.setAddress("O Strada");
        ownerInactive.setCity("Iasi");
        ownerInactive.setTelephoneNumber("0711111111");
        ownerInactive.setIsActive(false);
        ownerInactive.setPets(new HashSet<>());

        Owner ownerInactive1 = new Owner();
        ownerInactive1.setId(23L);
        ownerInactive1.setFirstName("James");
        ownerInactive1.setLastName("Doe");
        ownerInactive1.setAddress("Alta Strada");
        ownerInactive1.setCity("Iasi");
        ownerInactive1.setTelephoneNumber("0722222222");
        ownerInactive1.setIsActive(false);
        ownerInactive1.setPets(new HashSet<>());

        inactiveOwnerList = new ArrayList<>();
        inactiveOwnerList.add(ownerInactive);
        inactiveOwnerList.add(ownerInactive1);
    }

    @AfterEach
    public void teardown() {
        ownerList.clear();
        inactiveOwnerList.clear();
    }

    @Test
    void testFindActiveOwners() {
        when(ownerRepository.findByIsActive(anyBoolean())).thenReturn(ownerList);
        assertEquals(2, ownerServiceImpl.findActiveOwners().size());
        verify(ownerRepository, times(1)).findByIsActive(anyBoolean());
    }

    @Test
    void testFindActiveOwnersThrowsException() {
        when(ownerRepository.findByIsActive(anyBoolean())).thenThrow(new EmptyOwnerListException());
        EmptyOwnerListException exception = assertThrows(EmptyOwnerListException.class,
                () -> ownerServiceImpl.findActiveOwners());
        assertEquals("No owners found!", exception.getMessage());
        verify(ownerRepository, times(1)).findByIsActive(anyBoolean());
    }

    @Test
    void testFindInactiveOwners() {
        when(ownerRepository.findByIsActive(anyBoolean())).thenReturn(inactiveOwnerList);
        assertEquals(2, ownerServiceImpl.findInactiveOwners().size());
        verify(ownerRepository, times(1)).findByIsActive(anyBoolean());
    }

    @Test
    void testFindInactiveOwnersThrowsException() {
        when(ownerRepository.findByIsActive(anyBoolean())).thenThrow(new EmptyOwnerListException());
        EmptyOwnerListException exception = assertThrows(EmptyOwnerListException.class,
                () -> ownerServiceImpl.findInactiveOwners());
        assertEquals("No owners found!", exception.getMessage());
        verify(ownerRepository, times(1)).findByIsActive(anyBoolean());
    }

    @Test
    void testChangeOwnerIsActiveFlag() {
        long ownerId = 1L;

        Owner owner = new Owner();
        owner.setId(ownerId);
        owner.setIsActive(true);
        Optional<Owner> optionalOwner = Optional.of(owner);

        when(ownerRepository.findById(ownerId)).thenReturn(optionalOwner);
        ownerServiceImpl.changeOwnerIsActiveFlag(ownerId);

        Optional<Owner> actualOwner = ownerRepository.findById(ownerId);
        assertEquals(false, actualOwner.get().getIsActive());
    }

    @Test
    void testChangeOwnerIsActiveFlagThrowsException() {
        long ownerId = 100L;

        when(ownerRepository.findById(ownerId)).thenThrow(new NoOwnerFoundException(ownerId));
        NoOwnerFoundException exception = assertThrows(NoOwnerFoundException.class,
                () -> ownerServiceImpl.changeOwnerIsActiveFlag(ownerId));
        assertEquals("No owner with id " + ownerId + " found!", exception.getMessage());
        verify(ownerRepository, times(1)).findById(ownerId);
    }
}
