package com.petclinic.exceptions;

public class NoVetDescriptionFoundException extends RuntimeException {

    public NoVetDescriptionFoundException(long vetId) {
        super("Vet with id " + vetId + " doesn't have a description!");
    }
}
