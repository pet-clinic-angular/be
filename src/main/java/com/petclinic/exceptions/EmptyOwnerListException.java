package com.petclinic.exceptions;

public class EmptyOwnerListException extends RuntimeException {

    public EmptyOwnerListException() {
        super("No owners found!");
    }
}
