package com.petclinic.exceptions;

public class EmptyVetListException extends RuntimeException {

    public EmptyVetListException() {
        super("No vets found!");
    }
}
