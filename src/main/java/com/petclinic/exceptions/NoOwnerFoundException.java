package com.petclinic.exceptions;

public class NoOwnerFoundException extends RuntimeException {

    public NoOwnerFoundException(Long ownerId) {
        super("No owner with id " + ownerId + " found!");
    }
}
