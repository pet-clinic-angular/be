package com.petclinic.services.serviceImpl;

import com.petclinic.dto.OwnerDto;
import com.petclinic.exceptions.EmptyOwnerListException;
import com.petclinic.exceptions.NoOwnerFoundException;
import com.petclinic.mapper.OwnerMapper;
import com.petclinic.model.Owner;
import com.petclinic.repositories.OwnerRepository;
import com.petclinic.services.OwnerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class OwnerServiceImpl implements OwnerService {

    private final OwnerRepository ownerRepository;

    @Override
    public Set<OwnerDto> findActiveOwners() {
        Set<OwnerDto> activeOwnersForFrontEnd = new HashSet<>();

        List<Owner> ownerList = ownerRepository.findByIsActive(true);

        if (ownerList.isEmpty()) {
            throw new EmptyOwnerListException();
        } else {
            ownerList.forEach(owner -> activeOwnersForFrontEnd.add(OwnerMapper.ownerToOwnerDto(owner)));
        }
        return activeOwnersForFrontEnd;
    }

    @Override
    public Set<OwnerDto> findInactiveOwners() {
        Set<OwnerDto> inactiveOwnersForFrontEnd = new HashSet<>();

        List<Owner> ownerList = ownerRepository.findByIsActive(false);

        if (ownerList.isEmpty()) {
            throw new EmptyOwnerListException();
        } else {
            ownerList.forEach(owner -> inactiveOwnersForFrontEnd.add(OwnerMapper.ownerToOwnerDto(owner)));
        }
        return inactiveOwnersForFrontEnd;
    }

    @Override
    public void changeOwnerIsActiveFlag(Long ownerId) {
        Owner owner = ownerRepository.findById(ownerId).orElseThrow(() -> new NoOwnerFoundException(ownerId));
        boolean isActive = owner.getIsActive();
        owner.setIsActive(!isActive);
        ownerRepository.save(owner);
    }
}
