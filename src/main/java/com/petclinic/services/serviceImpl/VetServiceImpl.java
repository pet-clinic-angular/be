package com.petclinic.services.serviceImpl;

import com.petclinic.dto.VetDto;
import com.petclinic.exceptions.EmptyVetListException;
import com.petclinic.mapper.VetMapper;
import com.petclinic.model.Vet;
import com.petclinic.repositories.VetRepository;
import com.petclinic.services.VetService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class VetServiceImpl implements VetService {

    private final VetRepository vetRepository;

    @Override
    public Set<VetDto> findVets() {
        Set<VetDto> vetsForFrontEnd = new HashSet<>();

        List<Vet> vetList = vetRepository.findAll();

        if (vetList.isEmpty()) {
            throw new EmptyVetListException();
        } else {
            vetList.forEach(vet -> vetsForFrontEnd.add(VetMapper.vetToVetDto(vet)));
        }
        return vetsForFrontEnd;
    }
}
