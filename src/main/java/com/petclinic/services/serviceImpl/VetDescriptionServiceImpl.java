package com.petclinic.services.serviceImpl;

import com.petclinic.dto.VetDescriptionDto;
import com.petclinic.exceptions.NoVetDescriptionFoundException;
import com.petclinic.mapper.VetDescriptionMapper;
import com.petclinic.model.VetDescription;
import com.petclinic.repositories.VetDescriptionRepository;
import com.petclinic.services.VetDescriptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VetDescriptionServiceImpl implements VetDescriptionService {

    private final VetDescriptionRepository vetDescriptionRepository;

    @Override
    public VetDescriptionDto findVetDescription(long vetId, String language) {
        VetDescription vetDescription =
                vetDescriptionRepository.findByVetIdAndLanguage(vetId, language).orElseThrow(() -> new NoVetDescriptionFoundException(vetId));
        return VetDescriptionMapper.vetDescriptionToVetDescriptionDto(vetDescription);
    }
}
