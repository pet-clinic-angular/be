package com.petclinic.services;

import com.petclinic.dto.VetDto;

import java.util.Set;

public interface VetService {

    Set<VetDto> findVets();
}
