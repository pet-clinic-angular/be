package com.petclinic.services;

import com.petclinic.dto.VetDescriptionDto;

public interface VetDescriptionService {

    VetDescriptionDto findVetDescription(long vetId, String language);
}
