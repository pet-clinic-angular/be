package com.petclinic.services;

import com.petclinic.dto.OwnerDto;

import java.util.Set;

public interface OwnerService {

    Set<OwnerDto> findActiveOwners();

    Set<OwnerDto> findInactiveOwners();

    void changeOwnerIsActiveFlag(Long ownerId);
}
