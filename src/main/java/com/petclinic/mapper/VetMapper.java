package com.petclinic.mapper;

import com.petclinic.dto.VetDto;
import com.petclinic.model.Vet;

public class VetMapper {

    public static VetDto vetToVetDto(Vet vet) {
        return VetDto.vetDtoBuilder()
                .id(vet.getId())
                .firstName(vet.getFirstName())
                .lastName(vet.getLastName())
                .specialities(vet.getSpecialities())
                .build();
    }
}
