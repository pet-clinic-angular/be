package com.petclinic.mapper;

import com.petclinic.dto.OwnerDto;
import com.petclinic.model.Owner;

public class OwnerMapper {

    public static OwnerDto ownerToOwnerDto(Owner owner) {
        return OwnerDto.ownerDtoBuilder()
                .id(owner.getId())
                .firstName(owner.getFirstName())
                .lastName(owner.getLastName())
                .address(owner.getAddress())
                .city(owner.getCity())
                .phoneNumber(owner.getTelephoneNumber())
                .pets(owner.getPets())
                .build();
    }
}
