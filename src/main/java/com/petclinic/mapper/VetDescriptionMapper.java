package com.petclinic.mapper;

import com.petclinic.dto.VetDescriptionDto;
import com.petclinic.model.VetDescription;

public class VetDescriptionMapper {

    public static VetDescriptionDto vetDescriptionToVetDescriptionDto(VetDescription vetDescription) {
        return VetDescriptionDto.builder()
                .description(vetDescription.getDescription())
                .build();
    }
}
