package com.petclinic.repositories;

import com.petclinic.model.VetDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VetDescriptionRepository extends JpaRepository<VetDescription, Long> {

    Optional<VetDescription> findByVetIdAndLanguage(long vetId, String language);
}
