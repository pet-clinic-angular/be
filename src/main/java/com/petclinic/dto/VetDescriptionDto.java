package com.petclinic.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VetDescriptionDto {

    private String description;
}
