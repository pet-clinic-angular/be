package com.petclinic.dto;

import com.petclinic.model.Person;
import com.petclinic.model.Pet;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class OwnerDto extends Person {
    private String address;
    private String city;
    private String phoneNumber;
    private Set<Pet> pets;

    @Builder(builderMethodName = "ownerDtoBuilder")
    public OwnerDto(Long id, String firstName, String lastName, String address, String city, String phoneNumber,
                    Set<Pet> pets) {
        super(id, firstName, lastName);
        this.address = address;
        this.city = city;
        this.phoneNumber = phoneNumber;
        this.pets = pets;
    }
}
