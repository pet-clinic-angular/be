package com.petclinic.dto;

import com.petclinic.model.Person;
import com.petclinic.model.Speciality;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class VetDto extends Person {

    private Set<Speciality> specialities;

    @Builder(builderMethodName = "vetDtoBuilder")
    public VetDto(Long id, String firstName, String lastName, Set<Speciality> specialities) {
        super(id, firstName, lastName);
        this.specialities = specialities;
    }
}
