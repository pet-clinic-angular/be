package com.petclinic.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "specialities")
public class Speciality extends BaseEntity {

    @Column(name = "name")
    private String name;
}
