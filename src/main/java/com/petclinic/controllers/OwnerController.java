package com.petclinic.controllers;

import com.petclinic.dto.OwnerDto;
import com.petclinic.services.OwnerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class OwnerController {

    private final OwnerService ownerService;

    @GetMapping("owners")
    public Set<OwnerDto> findActiveOwners() {
        return ownerService.findActiveOwners();
    }

    @GetMapping("owners/inactive")
    public Set<OwnerDto> findInactiveOwners() {
        return ownerService.findInactiveOwners();
    }

    @PatchMapping("change-flag/{id}")
    public void changeOwnerIsActiveFlag(@PathVariable("id") Long id) {
        ownerService.changeOwnerIsActiveFlag(id);
    }
}
