package com.petclinic.controllers;

import com.petclinic.dto.VetDescriptionDto;
import com.petclinic.dto.VetDto;
import com.petclinic.services.VetDescriptionService;
import com.petclinic.services.VetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class VetController {

    private final VetService vetService;
    private final VetDescriptionService vetDescriptionService;

    @GetMapping("vets")
    public Set<VetDto> findVets() {
        return vetService.findVets();
    }

    @GetMapping("vet/{id}/{language}")
    public VetDescriptionDto findVetDescription(@PathVariable("id") long vetId, @PathVariable("language") String language) {
        return vetDescriptionService.findVetDescription(vetId, language);
    }
}
