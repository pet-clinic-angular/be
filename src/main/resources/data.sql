insert into owners (first_name, last_name, address, city, telephone_number, is_active)
values ('Bogdan', 'Cristea', 'Str. Bucium', 'Iasi', '0785123456', 1);
insert into owners (first_name, last_name, address, city, telephone_number, is_active)
values ('Andreea', 'Petcu', 'Str. Fulger', 'Iasi', '0784987654', 1);
insert into owners (first_name, last_name, address, city, telephone_number, is_active)
values ('Matei', 'Popescu', 'Str. Mihai Viteazu', 'Iasi', '0783567432', 1);
insert into owners (first_name, last_name, address, city, telephone_number, is_active)
values ('Liliana', 'Diaconu', 'Str. Bradului', 'Iasi', '0782123768', 1);
insert into owners (first_name, last_name, address, city, telephone_number, is_active)
values ('Alina', 'Brandusa', 'Str. Plopilor', 'Iasi', '0789432098', 0);

insert into pet_types (name)
values ('dog');
insert into pet_types (name)
values ('cat');

insert into pets (name, pet_type_id, owner_id, birth_date)
values ('Bella', 1, 1, '2020-08-11');
insert into pets (name, pet_type_id, owner_id, birth_date)
values ('Lulu', 1, 2, '2019-09-15');
insert into pets (name, pet_type_id, owner_id, birth_date)
values ('Sasha', 2, 3, '2015-05-03');
insert into pets (name, pet_type_id, owner_id, birth_date)
values ('Roco', 1, 3, '2014-12-19');
insert into pets (name, pet_type_id, owner_id, birth_date)
values ('Leia', 2, 4, '2018-02-20');

insert into specialities (name)
values ('Cardiology');
insert into specialities (name)
values ('Dermatology');
insert into specialities (name)
values ('Nutrition');

insert into vets (first_name, last_name)
values ('Costel', 'Baciu');
insert into vets (first_name, last_name)
values ('Andrei', 'Ciobutariu');
insert into vets (first_name, last_name)
values ('Maria', 'Iancu');
insert into vets (first_name, last_name)
values ('Elena', 'Sabau');

insert into vet_specialities (vet_id, speciality_id)
values (1, 1);
insert into vet_specialities (vet_id, speciality_id)
values (2, 1);
insert into vet_specialities (vet_id, speciality_id)
values (2, 3);
insert into vet_specialities (vet_id, speciality_id)
values (3, 2);
insert into vet_specialities (vet_id, speciality_id)
values (3, 3);
insert into vet_specialities (vet_id, speciality_id)
values (4, 2);
insert into vet_specialities (vet_id, speciality_id)
values (4, 3);

insert into visits (date, description, pet_id)
values ('2022-09-15', 'Routine check', 1);
insert into visits (date, description, pet_id)
values ('2022-09-16', 'Routine check', 2);
insert into visits (date, description, pet_id)
values ('2022-09-17', 'Routine check', 3);
insert into visits (date, description, pet_id)
values ('2022-09-18', 'Routine check', 4);
insert into visits (date, description, pet_id)
values ('2022-09-19', 'Routine check', 5);
insert into visits (date, description, pet_id)
values ('2022-10-15', 'Routine check', 1);
insert into visits (date, description, pet_id)
values ('2022-10-16', 'Routine check', 2);
insert into visits (date, description, pet_id)
values ('2022-10-17', 'Routine check', 3);
insert into visits (date, description, pet_id)
values ('2022-10-18', 'Routine check', 4);
insert into visits (date, description, pet_id)
values ('2022-10-19', 'Routine check', 5);

insert into vet_description (description, language, vet_id)
values ('I love working with animals and educating owners about their pet`s basic needs and treatments.', 'en', 1);
insert into vet_description (description, language, vet_id)
values ('Îmi place să lucrez cu animalele și să educ proprietarii cu privire la nevoile și tratamentele de bază ale
animalelor lor de companie.', 'ro', 1);
insert into vet_description (description, language, vet_id)
values ('I enjoy providing animals with medical and dietary care when necessary.', 'en', 2);
insert into vet_description (description, language, vet_id)
values ('Îmi place să ofer animalelor îngrijire medicală și dietetică atunci când este necesar.', 'ro', 2);
insert into vet_description (description, language, vet_id)
values ('I am an empathetic veterinarian trying to prevent and treat illnesses, improving animals health and well-being.', 'en', 3);
insert into vet_description (description, language, vet_id)
values ('Sunt un medic veterinar empatic care încearcă să prevină și să trateze bolile, îmbunătățind sănătatea și bunăstarea animalelor.', 'ro', 3);
insert into vet_description (description, language, vet_id)
values ('As a veterinarian I like giving recommendation`s to animal owners on ways to feed, clean, and care for animals to ensure their health.', 'en', 4);
insert into vet_description (description, language, vet_id)
values ('În calitate de medic veterinar îmi place să dau recomandări proprietarilor de animale cu privire la modalități de a hrăni, curăța și îngriji animalele pentru a le asigura sănătatea.', 'ro', 4);